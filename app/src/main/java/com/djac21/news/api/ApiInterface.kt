package com.djac21.news.api

import com.djac21.news.models.NewsModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("everything")
    suspend fun getNewsByCategory(
        @Query("apiKey") apiKey: String?,
        @Query("q") category: String?
    ): Response<NewsModel>
}