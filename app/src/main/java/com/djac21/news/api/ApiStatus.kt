package com.djac21.news.api

enum class ApiStatus {
    SUCCESS,
    ERROR,
    LOADING
}