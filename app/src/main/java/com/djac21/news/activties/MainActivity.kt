package com.djac21.news.activties

import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.djac21.news.R
import com.djac21.news.adapters.CenterZoomLayoutManager
import com.djac21.news.adapters.NewsAdapter
import com.djac21.news.api.ApiStatus
import com.djac21.news.models.ArticlesItem
import com.djac21.news.models.NewsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope

private val TAG = MainActivity::class.java.simpleName
private var newsList: MutableList<ArticlesItem> = mutableListOf()
private lateinit var newsAdapter: NewsAdapter
private lateinit var newsViewModel: NewsViewModel
private lateinit var linearLayoutManager: LinearLayoutManager
private var selectedCategory: String? = null

class MainActivity : AppCompatActivity(), CoroutineScope by MainScope() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        linearLayoutManager = CenterZoomLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.layoutManager = linearLayoutManager
        newsAdapter = NewsAdapter(newsList, this)
        recyclerView.adapter = newsAdapter
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val firstItemVisible = linearLayoutManager.findFirstVisibleItemPosition()
                val findFirstCompletelyVisibleItem = linearLayoutManager.findFirstCompletelyVisibleItemPosition()

                if (firstItemVisible != 1 && firstItemVisible % newsList.size == 1)
                    linearLayoutManager.scrollToPosition(1)

                if (findFirstCompletelyVisibleItem == 0)
                    linearLayoutManager.scrollToPositionWithOffset(newsList.size, 0)
            }
        })

        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)

        val categoryAdapter = ArrayAdapter.createFromResource(applicationContext, R.array.categories, R.layout.text_view_item)
        categoryAdapter.setDropDownViewResource(R.layout.text_view_item)

        val categorySpinner: Spinner = findViewById(R.id.spinner)
        categorySpinner.adapter = categoryAdapter
        categorySpinner.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.grey_500))
        categorySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedCategory = categorySpinner.selectedItem as String
                selectedCategory?.let { getNewsArticles(it) }
                setFooterImage()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        selectedCategory?.let { getNewsArticles(it) }

        retry_button.setOnClickListener {
            selectedCategory?.let { category -> getNewsArticles(category) }
        }
    }

    private fun getNewsArticles(category: String) {
        newsViewModel.getNews(category).observe(this, Observer {
            it?.let { apiResponse ->
                when (apiResponse.apiStatus) {
                    ApiStatus.LOADING -> {
                        showProgressBar()
                        recyclerView.visibility = View.GONE
                        error_layout.visibility = View.GONE
                    }
                    ApiStatus.SUCCESS -> {
                        recyclerView.visibility = View.VISIBLE
                        error_layout.visibility = View.GONE
                        hideProgressBar()

                        apiResponse.data?.let { response ->
                            newsList.clear()
                            response.body()?.articles?.let { articles ->
                                newsList.addAll(articles)
                            }

                            newsAdapter.apply {
                                notifyDataSetChanged()
                            }

                            recyclerView.post {
                                val dx = (recyclerView.width - recyclerView.getChildAt(0).width) / 2
                                recyclerView.scrollBy(-dx, 0)

                                recyclerView.onFlingListener = null
                                LinearSnapHelper().attachToRecyclerView(recyclerView)
                            }
                        }
                    }
                    ApiStatus.ERROR -> {
                        recyclerView.visibility = View.GONE
                        error_layout.visibility = View.VISIBLE
                        hideProgressBar()
                        Log.e(TAG, "Error: ${it.message}")
                    }
                }
            }
        })
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    private fun setFooterImage() {
        val categoryList = resources.getStringArray(R.array.categories)

        when (selectedCategory) {
            categoryList[0] -> {
                footer_image.setImageResource(R.drawable.nike_logo)
            }
            categoryList[1] -> {
                footer_image.setImageResource(R.drawable.snkrs_logo)
            }
            categoryList[2] -> {
                footer_image.setImageResource(R.drawable.phone)
            }
            categoryList[3] -> {
                footer_image.setImageResource(R.drawable.basketball)
            }
            categoryList[4] -> {
                footer_image.setImageResource(R.drawable.business)
            }
            categoryList[5] -> {
                footer_image.setImageResource(R.drawable.hospital)
            }
            categoryList[6] -> {
                footer_image.setImageResource(R.drawable.science)
            }
            categoryList[7] -> {
                footer_image.setImageResource(R.drawable.flight_takeoff)
            }
        }
    }
}