package com.djac21.news.models

data class NewsModel(
    val totalResults: Int = 0,
    val articles: List<ArticlesItem>?,
    val status: String = ""
)