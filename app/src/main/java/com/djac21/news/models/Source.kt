package com.djac21.news.models

data class Source(
    val name: String = "",
    val id: String = ""
)