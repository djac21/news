package com.djac21.news.models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.djac21.news.api.ApiClient
import com.djac21.news.api.ApiResponse
import com.djac21.news.utils.API_KEY
import kotlinx.coroutines.Dispatchers

class NewsViewModel : ViewModel() {
    fun getNews(category: String) = liveData(Dispatchers.IO) {
        emit(ApiResponse.loading(data = null))

        try {
            emit(ApiResponse.success(data = ApiClient.api.getNewsByCategory(API_KEY, category)))
        } catch (exception: Exception) {
            emit(ApiResponse.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}