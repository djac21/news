package com.djac21.news.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.djac21.news.R
import com.djac21.news.customTabs.CustomTabActivityHelper
import com.djac21.news.customTabs.CustomTabActivityHelper.openCustomTab
import com.djac21.news.customTabs.WebViewActivity
import com.djac21.news.models.ArticlesItem
import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class NewsAdapter(private val news: List<ArticlesItem>, private val context: Context) :
    RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val adjustedPosition = position % news.size

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var time: Long = 0
        try {
            time = simpleDateFormat.parse(news[adjustedPosition].publishedAt).time
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val prettyTime = PrettyTime(Locale.getDefault())
        holder.title.text = nullCheck(news[adjustedPosition].title)
        holder.author.text = nullCheck(news[adjustedPosition].author)
        holder.description.text = nullCheck(news[adjustedPosition].description)
        holder.date.text = nullCheck(prettyTime.format(Date(time)))

        Glide.with(context)
            .load(news[adjustedPosition].urlToImage)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_photo)
            )
            .into(holder.image)
    }

    private fun nullCheck(inputString: String?): String {
        return inputString ?: "N/A"
    }

    override fun getItemCount(): Int {
        return news.size * 2
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        var title: TextView = view.findViewById(R.id.title)
        var author: TextView = view.findViewById(R.id.author)
        var description: TextView = view.findViewById(R.id.description)
        var date: TextView = view.findViewById(R.id.date)
        var image: ImageView = view.findViewById(R.id.image)

        override fun onClick(view: View) {
            val position = absoluteAdapterPosition % news.size
            val url = news[position].url
            if (validateUrl(url)) {
                val uri = Uri.parse(url)
                uri?.let { openCustomChromeTab(it) }
            } else {
                Toast.makeText(context, "Error with link", Toast.LENGTH_SHORT).show()
            }
        }

        private fun validateUrl(url: String?): Boolean {
            return url != null && url.isNotEmpty() && (url.startsWith("http://") || url.startsWith("https://"))
        }

        private fun openCustomChromeTab(uri: Uri) {
            val intentBuilder = CustomTabsIntent.Builder()
            val customTabsIntent = intentBuilder.build()

            val params = CustomTabColorSchemeParams.Builder()
                .setNavigationBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setSecondaryToolbarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                .build()
            intentBuilder.setColorSchemeParams(CustomTabsIntent.COLOR_SCHEME_DARK, params)

            openCustomTab(context, customTabsIntent, uri, object : CustomTabActivityHelper.CustomTabFallback {
                override fun openUri(activity: Context?, uri: Uri?) {
                    openWebView(uri!!)
                }
            })
        }

        private fun openWebView(uri: Uri) {
            val webViewIntent = Intent(context, WebViewActivity::class.java)
            webViewIntent.putExtra(WebViewActivity.EXTRA_URL, uri.toString())
            webViewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(webViewIntent)
        }

        init {
            view.setOnClickListener(this)
        }
    }
}